1.  Enable multi currency: Setup -> Comapny Settings -> Company Information -> Edit -> Activate Multi Currencies

2. Reload page

3. Advanced currency management: Setup - Company Settings - Manage Currencies - Enable

4. Change Client ID and Secret Key for Connected App:
Setup - Apps - App Manager - View App

5. Add Currency Manager Data to tab in App "Currency Manager"

6. Add profile (sys admin) to App "Currency Manager"
Setup - Apps - App Manager - Edit - User profiles

6.1 add Custom settings "Fixer API" - API_KEY

7. Setup multi currencies

8. Reset user password (only for new environment scratch)

9. Security Token: Profile - Settings - Reset Security Token

