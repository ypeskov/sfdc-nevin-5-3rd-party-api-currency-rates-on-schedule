public class Peskov_Info_CurrencyRatesData {
    public Boolean success {get;set;}
    public Integer timestamp {get;set;} 
    public String base {get;set;}
    public Date currDate {get;set;}
    public Map<String, Decimal> rates {get;set;}
}
