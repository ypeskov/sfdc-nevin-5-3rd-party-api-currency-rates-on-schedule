global with sharing class Peskov_Info_CurrencyUpdater 
    implements Schedulable, Database.AllowsCallouts, Database.Batchable<SObject> {

    class Attributes {
        public String type {get;set;}
        public String referenceId {get;set;}
    }

    class CurrencyRate {
        public Attributes attributes {get;set;}
        public String IsoCode {get;set;}
        public Decimal ConversionRate {get;set;}
        public Date StartDate {get;set;}
    }

    static String sessionId;
    static Datetime nextRun;

    static String getSessionId() {
        Http h = new Http();
        HttpRequest req = new HttpRequest();

        String requestBody = 
            'grant_type=password' + 
            '&client_id=3MVG92H4TjwUcLlJXfo65UP1xOVy67qfVsvzhFXY3eJL2N2DwZxWe7WRfFEXeWqLGSEusuH.Vq88ztviNehtK' + 
            '&client_secret=A6AA8DC68662A0E61F4D5EDF820B71FBF416B1E7694A77D8594A9F6C2DC8FC91' + 
            '&username=test-hwxgdc7cn4wn@example.com' +
            '&password=Qwerty1238mvKGo0SfAR48lxD1l2bt7ya';

        req.setEndpoint('https://test.salesforce.com//services/oauth2/token');
        req.setBody(requestBody);
        req.setMethod('POST');
        
        HttpResponse res = h.send(req);
        
        Map<String, Object> paramsMap = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
        
        return (String) paramsMap.get('access_token');
    }

    /**
    * Initial start pattern: 
    * System.schedule('Init Job 4', '0 20 15 * * ? 2020', new Peskov_Info_CurrencyUpdater());
    **/
    global void execute(SchedulableContext ctx) {
        Id jobId = ctx.getTriggerId();
        System.abortJob(jobId);

        Database.executeBatch(new Peskov_Info_CurrencyUpdater());
    }

    public Iterable<SObject> start(Database.BatchableContext bc) {
        Datetime now = Datetime.now();
        Datetime nextRunTime = now.addHours(24);
        nextRun = nextRun;

        Peskov_Info_CurrencyUpdater.initUpdate();

        Peskov_Info_CurrencyUpdater.scheduleNext(nextRunTime, now);

        return new List<SObject>();
    }

    static void scheduleNext(Datetime nextRunTime, Datetime now) {
        String cronString = '' 
        + nextRunTime.second() + ' '
            + nextRunTime.minute() + ' '
            + nextRunTime.hour() + ' '
            + nextRunTime.day() + ' '
            + nextRunTime.month() + ' ' 
            + ' ? '
            + nextRunTime.year();

        System.schedule(Peskov_Info_CurrencyUpdater.class.getName() + '-' + now.format(), 
                        cronString, 
                        new Peskov_Info_CurrencyUpdater());

        System.debug(Datetime.now());
        System.debug('Running update');
        System.debug('---------------------------------------');
    }

    public void execute(Database.BatchableContext bc, List<SObject> scope) {}

    public void finish(Database.BatchableContext bc) {}

    @AuraEnabled()
    public static String initUpdate() {
        String result = '';
        try {
            String sessionId = Peskov_Info_CurrencyUpdater.getSessionId();
            Peskov_Info_CurrencyData currencyData = new Peskov_Info_CurrencyData();
            Peskov_Info_CurrencyRatesData currencyRatesData = currencyData.getData('fixer');
            CurrencyType[] activeCurrencies = Peskov_Info_CurrencyUpdater.getActiveCurrencies();
            List<CurrencyRate> ratesToSave = new List<CurrencyRate>();

            for(CurrencyType curr : activeCurrencies) {
                if (currencyRatesData.rates.containsKey(curr.IsoCode)) {
                    CurrencyRate rate = new CurrencyRate();
                    rate.attributes = new Attributes();
                    rate.attributes.type = 'DatedConversionRate';
                    rate.attributes.referenceId = curr.IsoCode;
                    rate.IsoCode = curr.IsoCode;
                    rate.ConversionRate = currencyRatesData.rates.get(curr.IsoCode);
                    rate.StartDate = currencyRatesData.currDate;
                    
                    ratesToSave.add(rate);
                }
            }

            String requestBody = '{"records":' + JSON.serialize(ratesToSave) + '}';

            Http h = new Http();
            HttpRequest req = new HttpRequest();
            req.setEndpoint(URL.getSalesforceBaseUrl().toExternalForm() + '/services/data/v47.0/composite/tree/DatedConversionRate/');
            req.setBody(requestBody);
            req.setHeader('Authorization', 'OAuth ' + sessionId);
            req.setHeader('Content-Type', 'application/json');
            req.setMethod('POST');
            
            HttpResponse res = h.send(req);
            // System.debug(res);
            If (res.getStatusCode() == 201) {
                Peskov_Info_CurrencyUpdater.saveUpdateResult(ratesToSave.size(), 
                                                activeCurrencies.size(), 
                                                '', 
                                                JSON.serialize(ratesToSave),
                                                true);
            } else {
                Peskov_Info_CurrencyUpdater.saveUpdateResult(0, 
                                                            0, 
                                                            res.getBody(),
                                                            '',
                                                            false);
            }

            result = res.getBody();
        } catch(Exception e) {
            Peskov_Info_CurrencyUpdater.saveUpdateResult(0, 0, e.getMessage(), '', false);
        }

        return result;
    }

    public static List<CurrencyType> getActiveCurrencies() {
        CurrencyType curr = Database.query('SELECT IsoCode FROM CurrencyType where IsCorporate = true');
        String corporateCurr = curr.IsoCode;

        String query = 'SELECT Id, IsoCode, IsCorporate ' + 
            'FROM CurrencyType ' +
            'WHERE isActive = true AND IsoCode != \'' + corporateCurr + '\'';

        CurrencyType[] activeCurrencies = Database.query(query);

        return activeCurrencies;
    }

    static void saveUpdateResult(Integer numberOfOrgCurrencies, 
                                Integer numberOfCurrenciesSaved, 
                                String errorMsg,
                                String updatedCurrencies,
                                Boolean result) {
        
        Boolean resultQty = true;

        if (result == true) {
            resultQty = numberOfCurrenciesSaved == numberOfOrgCurrencies;
        }

        if (resultQty == false) {
            String additionalErrorMsg = 'Number of org currencies (' + numberOfOrgCurrencies + ') and actual currencies (' +
                    numberOfCurrenciesSaved + ') is not the same.';
            errorMsg += ' | ' + additionalErrorMsg;
        }

        Currency_Manager_Data__c currencyData = 
            new Currency_Manager_Data__c(Number_of_currencies__c=numberOfCurrenciesSaved,
                                        Last_Update_Result__c=result,
                                        Error_Message__c=errorMsg,
                                        Updated_Currencies__c=updatedCurrencies,
                                        Next_Run__c=nextRun,
                                        Date_Updated__c=Datetime.now());
            insert currencyData;
    }

    @AuraEnabled(cacheable=true)
    public static Currency_Manager_Data__c getLastUpdateInfo() {
        return [SELECT Name, 
                        Date_Updated__c, 
                        Number_of_currencies__c, 
                        Error_Message__c,
                        Updated_Currencies__c,
                        Last_Update_Result__c 
                FROM Currency_Manager_Data__c 
                ORDER BY Date_Updated__c 
                DESC LIMIT 1];
    }

    @AuraEnabled()
    public static String clearDatedConversionRate() {
        String sessionId = Peskov_Info_CurrencyUpdater.getSessionId();

        DatedConversionRate[] rates = (List<DatedConversionRate>) Database.query('SELECT Id FROM DatedConversionRate');
        
        Id[] ids = new List<Id>();
        for(DatedConversionRate rate : rates) {
            ids.add(rate.Id);
        }

        String params = 'ids=' + String.join(ids, ',');

        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(URL.getSalesforceBaseUrl().toExternalForm() + '/services/data/v47.0/composite/sobjects/?' + params);
        req.setHeader('Authorization', 'OAuth ' + sessionId);
        req.setMethod('DELETE');
        
        HttpResponse res = h.send(req);

        return res.getBody();
    }
}
