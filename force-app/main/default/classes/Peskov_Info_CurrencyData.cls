public with sharing class Peskov_Info_CurrencyData {
    public class InvalidCurrencyRatesProviderException extends Exception {}

    public Peskov_Info_CurrencyRatesData getData(String provider) {
        provider = provider.toLowerCase();

        Peskov_Info_CurrencyRatesData data;

        switch on provider {
            when 'fixer' {
                Peskov_Info_FixerAPI api = new Peskov_Info_FixerAPI();
                data = api.getCurrencyData();
            }
            when else {
                throw new InvalidCurrencyRatesProviderException('Unknown Currency Source Provider');
            }
        }

        return data;
    }
}
