/* eslint-disable no-unused-vars */
/* eslint-disable no-console */
import { LightningElement, track } from 'lwc';
import initUpdate from '@salesforce/apex/Peskov_Info_CurrencyUpdater.initUpdate';
import clearDatedConversionRate from '@salesforce/apex/Peskov_Info_CurrencyUpdater.clearDatedConversionRate';
import getLastUpdateInfo from '@salesforce/apex/Peskov_Info_CurrencyUpdater.getLastUpdateInfo';

export default class Peskov_Info_CurrencyRates extends LightningElement {
    @track lastUpdateInfo = {
        Name: '',
        Date_Updated__c: '',
        Number_of_currencies__c: 0,
        Last_Update_Result: '',
        Updated_Currencies__c: '',
        Error_Message__c: '',
    };

    @track lastUpdatedRates = [];

    connectedCallback() {
        getLastUpdateInfo()
        .then(result => {
            this.lastUpdateInfo = result;
            this.lastUpdatedRates = JSON.parse(this.lastUpdateInfo.Updated_Currencies__c);
        })
        .catch(error => console.log(error));
    }

    get updateResult() {
        return this.lastUpdateInfo.Last_Update_Result__c === true ? 'Success' : 'Fail';
    }

    get hasErrorMessage() {
        return this.lastUpdateInfo.Error_Message__c && this.lastUpdateInfo.Error_Message__c.length > 0;
    }

    handleUpdate() {
        initUpdate()
        .then(result => {
            // console.log(result);
        })
        .catch(error => console.log(error));
    }

    handleDelete() {
        clearDatedConversionRate()
        .then(result => {
            console.log(result);
        })
        .catch(error => console.log(error));
    }
}